const mix = require('laravel-mix');
const fs = require('fs');

mix
    .js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .copy(['fonts'], 'public/fonts')
    .copy(['images'], 'public/images')
    .then(() => {
        try {
            fs.rmdirSync('fonts', { recursive: true });
            fs.rmdirSync('images', { recursive: true });
        } catch (err) {
            console.error(err.message);
        }
    });
