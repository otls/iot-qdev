import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import bodyParser from "body-parser";
import handlebars from 'express-handlebars';
require('dotenv').config();

var app = express();

app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.set('views', path.join(__dirname, 'app/views'));
app.set('view engine', 'hbs');

app.engine('hbs', handlebars({
    layoutsDir: __dirname + '/app/views/layouts',
    extname: 'hbs',
    defaultLayout: 'main',
    helpers: {
        asset:
            (targetFile) => {
                return `/${targetFile}`;
            },
        env:
            (varname, altName) => {
                return process.env[varname] || altName;
        }
    }
}));
module.exports = app;