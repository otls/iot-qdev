const path = require('path');
const fs = require('fs');

function traverseDir(dir = false, app = '') {
    if (!dir) {
        dir = __root + "/routes";
    }
    fs.readdirSync(dir).forEach(file => {
        let fullPath = path.join(dir, file);
        if (fs.lstatSync(fullPath).isDirectory()) {
            traverseDir(fullPath, app);
        } else {
            let routeFile = require(fullPath);
            app.use(routeFile.routePath, routeFile.routes);
        }
    });
}

exports.initRoutes = (app) => {
    traverseDir(false, app);
}
