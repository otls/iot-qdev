global.hash = {
    make: async (plaintext) => {

        const bcrypt = require('bcrypt');
        const salt = await bcrypt.genSalt(parseInt(process.env.HASH_ROUND));

        return await bcrypt.hash(plaintext, salt);
    },
    compare: async (password, hash) => {
        const bcrypt = require('bcrypt');
        return await bcrypt.compare(password, hash);
    }
}
