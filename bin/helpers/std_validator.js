import { validationResult } from 'express-validator';
import { users } from '../../app/models';

global.std_validate = (req) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        return {
            success: true
        };
    }
    const errorMessages = [];
    errors.array().map((error) => errorMessages.push({ [error.param]: error.msg }));

    return {
        success: false,
        messages: errorMessages
    };
}

global.user = {
    exists: async (whereClause) => {
        return await users.findOne({ where: whereClause })
            .then(user => {
                return user ? true : false;
            })
    }
}
