global.jwt = {
    make: (payload) => {
        const jwt = require('jsonwebtoken');
        const secret = process.env.JWT_SECRET;
        return jwt.sign(payload, secret);
    }
}