const path = require('path');

global.loadController = (controllerName) => {
    return require(path.resolve(__controllers, controllerName));
}

global.loadRequest = (requestName) => {
    return require(path.resolve(__requests, requestName));
}

global.loadRoute = (routeName) => {
    return require(path.resolve(__routes, routeName));
}

global.loadModel = (modelName = false) => {
    const models = require(path.resolve(__models));
    return modelName ? models[modelName] : models;
}

global.loadHelper = (helperName) => {
    return require(path.resolve(__helpers, helperName));
}