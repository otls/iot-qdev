const base = require('path').resolve(process.cwd(), '');

global.__root = base;
global.__models = base + "/models";
global.__controllers = base + "/controllers";
global.__helpers = base + "/helpers";
global.__requests = base + "/requests";
global.__routes = base + "/routes";
global.__config = base + "/app/config";