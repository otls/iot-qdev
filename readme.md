# IoT Qdev
Based on [Exp-awesome](https://gitlab.com/otls/exp-awesome)  starter

### Installation
- npm install
```sh
$ cd [project dir]
$ npm install
```
- In the root folder copy .env.example to .env
- Set Variable in .env as your needs

- Run migration
```sh
$ cd [project dir]
$ npx sequelize db:migrate
```

- Run server
```sh
$ cd [project dir]
$ npm run server
```

- You are awesome to go

- To play with asset / resource you can find in /resources. to compile it, the followong command is what you have to run
```sh
$ cd [project dir]
$ npm run style-dev
```

### Dependencies
- [express](https://github.com/expressjs/express)
- [Sequelize](https://github.com/sequelize/sequelize)
- [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken#readme)
- [Bcrypt](https://github.com/kelektiv/node.bcrypt.js#readme)
- [body-parser](https://github.com/expressjs/body-parser#readme)
- [mysql2](https://github.com/sidorares/node-mysql2#readme)
- [dotenv](https://github.com/motdotla/dotenv#readme)
- [express-validator](https://github.com/express-validator/express-validator)
- [cookie-parser](https://github.com/expressjs/cookie-parser#readme)
- [babel](https://github.com/babel/babel)
- [express-handlebars](https://github.com/express-handlebars/express-handlebars)
- [handlebars](https://github.com/handlebars-lang/handlebars.js)
- [laravel mix](https://laravel-mix.com/docs/main/what-is-mix)
### Built-in

- API user JWT login
- API user Registration
- Loader (controller, model, helper, request)
- JWT Handler
- Hash Handler
- Config Handler
- Requests (validators)
- Router autoload
