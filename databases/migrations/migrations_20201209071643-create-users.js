'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(20)
      },
      usr_name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      usr_username: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      usr_email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      usr_password: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      usr_phone: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      usr_verified_at: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('users');
  }
};