import { body } from "express-validator";

const rules = () => {
    return [
        body('usr_username')
            .notEmpty()
            .isString()
            .isLength({
                max: 50,
                min: 4
            })
            .trim()
            .escape()
            .custom(async (value, { req }) => {
                if (await user.exists({usr_username: value})) {
                    return Promise.reject('Username already in use');
                }
            })
        ,
        body('usr_password')
            .notEmpty()
            .isString()
            .isLength({
                max: 50,
                min: 8
            })
            .trim()
            .escape()
        ,
        body('usr_name')
            .notEmpty()
            .isString()
            .isLength({
                min: 3,
                max:100
            })
            .trim()
            .escape()
        ,
        body('usr_email')
            .notEmpty()
            .isEmail()
            .custom(async (value, { req }) => {
                if (await user.exists({ usr_email: value })) {
                    return Promise.reject('Email already in use');
                }
            })
        ,
        body('usr_phone')
            .notEmpty()
            .isString()
            .isLength({
                min: 10,
                max: 16
            })
            .custom(async (value, { req }) => {
                if (await user.exists({ usr_phone: value })) {
                    return Promise.reject('Phone already in use');
                }
            })

    ];
}

const validate = (req, res, next) => {
    const validation = std_validate(req);
    console.log(validation);
    if (validation.success) {
        return next();
    }
    res.status(422).json(validation);
}

module.exports = {
    rules,
    validate
}
