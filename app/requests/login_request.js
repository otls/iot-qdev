import { body } from "express-validator";

const rules = () => {
    return [
        body('username')
            .notEmpty()
            .isString()
            .isLength({
                max: 50
            })
            .trim()
            .escape()
        ,
        body('password')
            .notEmpty()
            .isString()
            .isLength({
                max: 50
            })
            .trim()
            .escape()

    ];
}

const validate = (req, res, next) => {
    const validation = std_validate(req);
    if (validation.success) {
        return next();
    }
    res.status(422).json(validation);
}

module.exports = {
    rules,
    validate
}
