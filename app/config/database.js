require('dotenv').config();
module.exports = {
  "development": {
    "username": process.env.DEVDB_USERNAME,
    "password": process.env.DEVDB_PASSWORD,
    "database": process.env.DEVDB_NAME,
    "host": process.env.DEVDB_HOST,
    "dialect": process.env.DEVDB_DIALECT
  },
  "test": {
    "username": process.env.TESTDB_USERNAME,
    "password": process.env.TESTDB_PASSWORD,
    "database": process.env.TESTDB_NAME,
    "host": process.env.TESTDB_HOST,
    "dialect": process.env.TESTDB_DIALECT
  },
  "production": {
    "username": process.env.PRODDB_USERNAME,
    "password": process.env.PRODDB_PASSWORD,
    "database": process.env.PRODDB_NAME,
    "host": process.env.PRODDB_HOST,
    "dialect": process.env.PRODDB_DIALECT
  }
}

