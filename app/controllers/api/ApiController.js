class ApiController {

    constructor() {
        this.response = {
            message: 'success',
            data: [],
            code: 200
        }
    }
  
    setSuccessResponse (res = { message: 'Success', data: [], ret: false, additional: {} }) {
        this.response = {
            message: res.message || "success",
            data: res.data || [],
            code: res.code || 200,
            ...res.additional
        };

        if (res.ret) {
            return response;
        }
    }
    setFailedResponse (res = { message: 'Operation failed', data: [], ret: false, additional: {} }) {
        this.response = {
            message: res.message || "Operation failed",
            data: res.data || [],
            code: res.code || 500,
            ...res.additional
        };

        if (res.ret) {
            return response;
        }
    }

    sendUnauthorized (res, message = "Unauthorized") {
        return res.status(401).json({
            success: false,
            message: message,
            code: 401
        });
    }


    sendResponse (res, data = {}) {
        this.response = {
            ...this.response,
            ...data
        }
        res.status(this.response.code).json(this.response);
    }


}

export default ApiController;
