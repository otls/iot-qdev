import ApiController from "../ApiController";
import { users } from '../../../models';

class ApiRegisterController extends ApiController{

    async register(req, res) {
        try {
            const body = req.body;
            
            const user = await users.create({
                'usr_name': body.usr_name,
                'usr_username': body.usr_username,
                'usr_email': body.usr_email,
                'usr_phone': body.usr_phone,
                'usr_password': await hash.make(body.usr_password),
            });
            let payload = {
                'user': await hash.make(user.usr_username),
                'uid': user.id,
                'name': user.usr_name,
                'at': new Date()
            }
            let token = jwt.make(payload);
            user.usr_password = "Damn you see me, please tell noone!";
            
            this.setSuccessResponse({
                data: {
                    token: token,
                    user: user
                }
            });
        } catch (error) {
            this.setFailedResponse(error.message);
        }
        return this.sendResponse(res);
  }
}

export default ApiRegisterController;
