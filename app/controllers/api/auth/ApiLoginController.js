import { users } from '../../../models';
import ApiController from '../ApiController';

class ApiLoginController extends ApiController{

    async login (req, res, next) {

        const { username, password } = req.body;
        let user = await users.scope('withPassword').findOne({
            where: {
                usr_username: username
            }
        });

        if (!user || ! await hash.compare(password, user.usr_password)) {
            return this.sendUnauthorized(res, "Username or password is incorrect!");
        }

        let payload = {
            'user': await hash.make(user.usr_username),
            'uid': user.id,
            'name': user.usr_name,
            'at': new Date()
        }
        let token = jwt.make(payload);
        this.setSuccessResponse({
            data: {
                token: token
            }
        });
        return this.sendResponse(res);
    }
}

export default ApiLoginController;
