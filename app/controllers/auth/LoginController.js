// import { Users } from '../../models';

class LoginController {

  index(req, res)  {
    res.render('auth/login', {
      layout: 'login_layout'
    });
  }
  async login(req, res) {
    return res.json();
  }
}


export default new LoginController();
