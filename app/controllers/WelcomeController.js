class WelcomeController {
  async index(req, res) {
    return res.render('welcome', {title: "Welcome to Expa-adminlte"});
  }
}

export default new WelcomeController();
