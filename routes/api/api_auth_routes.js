import { Router } from 'express';
import LoginController from '../../app/controllers/api/auth/ApiLoginController';
import RegsiterController from '../../app/controllers/api/auth/ApiRegisterController';
import LoginRequest from '../../app/requests/login_request';
import RegisterRequest from '../../app/requests/register_request';


const routes = new Router();
const login = new LoginController();
const register = new RegsiterController();

routes.post(
    //  route
    '/login',
    // validation rules
    LoginRequest.rules(),
    // validation func
    LoginRequest.validate,
    // login controller
    (req, res, next) => {
        login.login(req, res, next);
    }
);

routes.post('/logout', (req, res, next) => {
    res.status(200).json({ message: "Logout" });
});

routes.post('/register', RegisterRequest.rules(), RegisterRequest.validate, (req, res, next) => register.register(req, res))

module.exports = {
    'routes': routes,
    'routePath': '/api/auth/'
};