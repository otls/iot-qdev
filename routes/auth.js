import { Router } from 'express';
import LoginController from '../app/controllers/auth/LoginController';
import LoginRequest from '../app/requests/login_request';

const routes = new Router();

// Add routes
routes.get('/login', LoginController.index);
routes.post('/login', LoginRequest.rules(), LoginRequest.validate, LoginController.login);

module.exports = {
    routes: routes,
    routePath: "/auth"
};
